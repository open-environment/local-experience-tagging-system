Summary: Creating a governance structure for managing this information and could have a build in to the technical system and governance of community data hubs. This would be a way for people to clearly mark who information is able to be shared with and how the community has designated places for potential use.

What’s the idea?
Traditional knowledge tagging system for environmental data atlas which tests new socio-technical features around sensitivity and privacy for community-generated data. 

Example: With fishing communities in the Louisiana Gulf Coast, we create a "farmers almanac" type local knowledge hub where oral history, lived ecological knowledge and other types of qualitative information can live. In parallel OEDP creates governance structures that help contributors to collectively govern who manages access to and how that access happens to the data. 
